import allure
import jsonpath

from utils.database_unit import DataBaseUtils
from config import setting
from request.Request import Request


class AssertUtils:
    '''
    封装各种断言类，包括json断言，文本断言，数据库断言
    '''

    def __init__(self):
        pass

    @allure.step('断言text')
    def assert_text(self, actual, expect):
        '''
        断言接口返回中是否包含指定文本
        '''
        if not expect == '':
            return expect in actual
        else:
            return True

    @allure.step('断言json')
    def assert_json(self, actual, expect):
        '''
        断言接口返回中是否包含指定jsonpath
        '''
        if expect.startswith('$.'):
            return self.assert_json_by_jsonpath(actual, expect)
        elif not expect == '':
            json_node = expect.split(',')[0]
            value = expect.split(',')[1]
            return self.assert_json_value(actual, json_node, value)
        else:
            return True

    @allure.step('断言sql数据')
    def assert_sql(self, text):
        '''
        断言数据库中的指定值是否一致
        '''
        if not text == '':
            sql = text.split(',')[0]
            except_value = text.split(',')[1]
            actual_value = DataBaseUtils().select(sql)
            return except_value in str(actual_value)
        else:
            return True

    @allure.step('断言jsonpath')
    def assert_json_by_jsonpath(self, actual, expect):
        '''
        通过jsonpath断言json数据
        '''
        if not expect == '':
            json_path = expect.split(',')[0]
            except_value = expect.split(',')[1]
            actual_val = jsonpath.jsonpath(actual, json_path)
            return except_value in str(actual_val)
        else:
            return True

    @allure.step('断言自定义json值')
    def assert_json_value(self, json_text, json_node, value):
        '''
        通过指定的json节点断言数据
        '''
        json_node_list = json_node.split('.')
        for i in range(len(json_node_list)):
            current_node = json_node_list[i]
            if '[' in current_node:
                key = current_node.split('[')[0]
                condition = current_node.split('[')[1].replace(']', '')
                condition_key = condition.split('=')[0]
                condition_value = condition.split('=')[1]
                list = json_text[key]
                for no in range(len(list)):
                    if list[no][condition_key] == condition_value:
                        json_text = list[no]
                        break
            else:
                json_text = json_text[current_node]
        if json_text == value:
            return True
        else:
            return False

    @allure.step('断言gemi绑定结果')
    def assert_bind_success_in_gemi(self, collector_code, except_status):
        '''
        绑定采集器以后断言硬件中台是否绑定成功
        '''
        post_url = setting.collector_bind_in_gemi_api.format(collector_code)
        header = {
            'token': 'ce3Bc2a26D14418Cac8450c8Fd5F336e'
        }
        r = Request().get(url=post_url, header=header)
        if r.json()['result']['labels']['bindStatus'] == except_status:
            return True
        else:
            return False


if __name__ == '__main__':
    print(AssertUtils().assert_bind_success_in_gemi('xyzbxpp', 'unbind'))