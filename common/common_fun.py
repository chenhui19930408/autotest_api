from datetime import datetime

import allure

from common.user_info import UserInfo
from request.Request import Request
from request.RequestUrl import RequestUrl


class CommonFun:

    def get_header(self):
        header = {
            'Content-Type': 'application/json;charset=UTF-8',
            'orgcode': '9001',
            'accessToken': UserInfo.get_access_token(),
            'clientId': UserInfo.client_id,
            'userid': UserInfo.get_user_id()
        }
        return header

    @allure.step('获取当前用户数据')
    def get_current_user_data(self):
        post_url = RequestUrl.get_current_user_data
        header = self.get_header()
        r = Request().postJson(url=post_url, header=header)
        return r.json()['res']

    @allure.step('新增字典管理并且获取ID')
    def add_dict_and_get_id(self, name='自动化测试数据', code='0001', sortNum='1', remark='', category='post_type'):
        post_url = RequestUrl.add_dict
        post_data = {
            "name": name,
            "code": code,
            "sortNum": sortNum,
            "remark": remark,
            "category": category
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询字典管理')
    def search_dict(self, category):
        post_url = RequestUrl.search_dict
        post_data = {
            "category": category
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('通过名称查询班制类别')
    def search_shift_type_by_name(self, name):
        result = None
        post_url = RequestUrl.search_dict
        post_data = {
            "category": 'shift_type'
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        result_list = r.json()['res']
        for i in range(len(result_list)):
            if result_list[i]['name'] == name:
                result = result_list[i]
        return result

    @allure.step('删除字典管理')
    def delete_dict(self, id):
        post_url = RequestUrl.delete_dict
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('查询计量单位')
    def search_unit(self, name=''):
        post_url = RequestUrl.search_unit
        post_data = {
            "name": name,
            "pageSize": 20,
            "pageIndex": 1
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('添加车间并获取ID')
    def add_workshop_and_get_id(self, name, code):
        post_url = RequestUrl.add_workshop
        post_data = {
            "enableState": 1,
            "code": code,
            "name": name,
            "leaderName": "侯都一工厂",
            "leaderId": 3
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询车间')
    def search_workshop(self):
        post_url = RequestUrl.search_workshop
        post_data = {
            "pageIndex": 1,
            "pageSize": 20,
            "enableState": None,
            "name": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除车间')
    def delete_workshop(self, id):
        post_url = RequestUrl.delete_workshop
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增班组并且返回班组ID')
    def add_group_team_and_get_id(self, name, code):
        post_url = RequestUrl.add_group_team
        post_data = {
            "enableState": 1,
            "code": code,
            "name": name
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询班组信息')
    def search_group_team(self):
        post_url = RequestUrl.search_group_team
        post_data = {
            "auditState": 3,
            "enableState": 1
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除班组')
    def delete_group_team(self, group_id):
        post_url = RequestUrl.delete_group_team
        post_data = {
            "id": group_id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增管圈颜色')
    def add_color_and_get_id(self, name, code):
        post_url = RequestUrl.add_color
        post_data = {
            "id": "",
            "name": name,
            "code": code,
            "typeId": 46,
            "typeName": "粗纱管",
            "colorValue": "#FFFFFF",
            "remark": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除管圈颜色')
    def delete_color(self, id):
        post_url = RequestUrl.delete_color
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('清空单日排班')
    def delete_daily_schedule(self, day='2021-07-21'):
        post_url = RequestUrl.make_schedule
        post_data = {
            "belongDates": [
                day
            ],
            "workshopId": 1,
            "workshopName": "一分厂",
            "shiftTypeId": 27,
            "shiftTypeName": "全日制",
            "shifts": [
            {
                    "groupIds": [],
                    "groups": [],
                    "shiftId": 10,
                    "shiftName": "日班"
                }
            ]
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('删除人员替岗')
    def delete_replace_post(self, id):
        post_url = RequestUrl.delete_replace_post
        post_data = [id]
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增班制并且获取ID')
    def add_shift_and_get_id(self):
        shift_type = self.search_shift_type_by_name('两班制')
        shift_type_id = shift_type['id']
        shift_type_name = shift_type['name']
        post_url = RequestUrl.add_shift
        post_data = {
            "name": "自动化测试数据",
            "typeId": shift_type_id,
            "typeName": shift_type_name,
            "beginHour": "1434",
            "endHour": "1534",
            "workingHours": 100,
            "remark": "",
            "auditState": 1,
            "createId": 6,
            "createName": "系统管理员",
            "createTime": "2021-07-13 15:56:36",
            "updateId": 6,
            "updateName": "系统管理员",
            "updateTime": "2021-07-13 15:56:36",
            "auditTime": "2021-07-13 15:56:36",
            "auditId": 6,
            "auditName": "系统管理员"
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询班制')
    def search_shift(self, type_id=None, name=''):
        post_url = RequestUrl.search_shift
        post_data = {
            "pageIndex": 1,
            "pageSize": 20,
            "typeId": type_id,
            "name": name
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除班制')
    def delete_shift(self, class_id):
        post_url = RequestUrl.delete_shift
        post_data = {
            "id": class_id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增生产设备并且返回设备ID')
    def add_equipment_and_get_id(self, name='自动化测试数据', code='test001', process=5):
        post_url = RequestUrl.add_update_equipment
        post_data = {
            "typeId": 28,
            "typeCode": "01",
            "typeName": "生产主机",
            "code": code,
            "name": name,
            "processId": process,
            "machineModelId": 4,
            "machineModelCode": "F1520",
            "machineModelName": "F1520",
            "workshopId": 1,
            "workshopName": "一分厂",
            "spinCount": 109,
            "prodTime": "2021-06-27 00:00:00",
            "manufacturer": "",
            "autoGather": 1,
            "outputRatio": 1.09,
            "isSpinOutput": 1,
            "isScheduling": True,
            "maxOutputQty": 223,
            "factoryCode": "",
            "installDate": "",
            "intoProductionDate": "",
            "processCode": "XS",
            "processName": "细纱"
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询生产设备并返回所有设备ID')
    def search_equipment_ids(self, name='', typeId=None, processId='', workshopId=1):
        result = []
        post_url = RequestUrl.search_equipment
        post_data = {
            "pageIndex": 1,
            "pageSize": 100,
            "name": name,
            "typeId": typeId,
            "processId": processId,
            "workshopId": workshopId
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        equipments = r.json()['res']
        for i in range(len(equipments)):
            result.append(equipments[i]['id'])
        return result

    @allure.step('删除生产设备')
    def delete_equipment(self, id):
        post_url = RequestUrl.delete_equipment
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增员工')
    def add_user_and_get_id(self, name, code):
        # 获取部门信息
        dept = self.search_department()[0]
        deptId = dept['id']
        deptName = dept['name']
        # 获取岗位信息
        post = self.search_post_infos()[0]
        postId = post['id']
        postName = post['name']
        post_url = RequestUrl.add_user
        post_data = {
            "id": None,
            "code": code,
            "name": name,
            "userId": None,
            "sortNum": 1,
            "deptId": deptId,
            "deptName": deptName,
            "workshopId": None,
            "workshopName": "5车间",
            "groupId": None,
            "groupName": "甲班",
            "idCard": "",
            "mobile": "",
            "email": "",
            "gender": 1,
            "tel": "",
            "leaderId": None,
            "leaderName": None,
            "onJob": True,
            "birthday": "",
            "postId": postId,
            "postName": postName,
            "remark": "",
            "createId": 6,
            "createName": "系统管理员",
            "createTime": "2021-07-12 16:42:51",
            "updateId": 6,
            "updateName": "系统管理员",
            "updateTime": "2021-07-12 16:42:51",
            "bindUser": 0,
            "loginName": "",
            "password": None,
            "loginTime": None,
            "shiftName": None,
            "userName": None,
            "imageIds": [],
            "imageList": []
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('获取所有员工信息')
    def get_user_infos(self, onJob=True, name='', deptId=None, workshop=None):
        post_url = RequestUrl.emp_list
        post_data = {
            "pageSize": 20,
            "pageIndex": 1,
            "onJob": onJob,
            "name": name,
            "deptId": deptId,
            "workshop": workshop
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除员工')
    def delete_user(self, id):
        post_url = RequestUrl.delete_user
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('获取工序信息')
    def get_process_infos(self):
        post_url = RequestUrl.get_process
        post_data = {
            "pageIndex": 1,
            "pageSize": 20,
            "name": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('新增岗位管理')
    def add_post_and_get_id(self, name, code):
        type = self.search_dict('post_type')[0]
        post_url = RequestUrl.add_post
        post_data = {
            "id": None,
            "name": name,
            "code": code,
            "sortNum": 5,
            "typeId": type['id'],
            "typeName": type['name'],
            "isWatcher": True,
            "isRegularDaily": True,
            "processId": None,
            "processCode": "",
            "processName": "",
            "wageType": 1,
            "isMainPost": False
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询岗位管理')
    def search_post_infos(self):
        post_url = RequestUrl.search_post
        post_data = {
            "pageIndex": 1,
            "pageSize": 20,
            "typeId": None,
            "processId": None,
            "name": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除岗位管理')
    def delete_post(self, id):
        post_url = RequestUrl.delete_post
        post_data = [id]
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增部门管理')
    def add_department_and_get_id(self, name='自动化测试数据', code='0123'):
        user = self.get_user_infos()[0]
        user_id = user['id']
        user_name = user['name']
        post_url = RequestUrl.add_department
        post_data = {
            "name": name,
            "code": code,
            "enableState": 1,
            "sortNum": 0,
            "parentId": 0,
            "leaderId": user_id,
            "leaderName": user_name
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询部门管理')
    def search_department(self):
        post_url = RequestUrl.search_department
        header = self.get_header()
        r = Request().postJson(url=post_url, header=header)
        return r.json()['res']

    @allure.step('删除部门管理')
    def delete_department(self, id):
        post_url = RequestUrl.delete_department
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增物料分类')
    def add_product_type_and_get_id(self, name='自动化测试数据', code='012345'):
        type = self.search_dict('stock_type')[0]
        type_id = type['id']
        type_name = type['name']
        post_url = RequestUrl.add_product_type
        post_data = {
            "name": name,
            "code": code,
            "typeId": type_id,
            "typeName": type_name,
            "enableBatch": True,
            "id": None,
            "parentId": 1
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询物料分类')
    def search_product_type(self, parent_id=None):
        post_url = RequestUrl.search_product_type
        post_data = {
            "parentId": parent_id
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除物料分类')
    def delete_product_type(self, id):
        post_url = RequestUrl.delete_product_type
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('新增物料属性')
    def add_product_property_and_get_id(self, name, code):
        type = self.search_dict('property_type')[0]
        type_id = type['id']
        type_name = type['name']
        post_url = RequestUrl.add_product_property
        post_data = {
            "id": "",
            "name": name,
            "code": code,
            "typeId": type_id,
            "typeName": type_name
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('查询物料属性')
    def search_product_property(self):
        post_url = RequestUrl.search_product_property
        post_data = {
            "pageSize": 20,
            "pageIndex": 1,
            "typeId": "",
            "name": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除物料属性')
    def delete_product_property(self, id):
        post_url = RequestUrl.delete_product_property
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('查询物料档案')
    def search_product_manage(self):
        post_url = RequestUrl.search_product_manage
        post_data = {
            "pageSize": 20,
            "pageIndex": 1,
            "categoryId": None,
            "auditState": None,
            "name": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除物料档案')
    def delete_product_manage(self, id):
        post_url = RequestUrl.delete_product_manage
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('查询批号管理')
    def search_batch_manage(self, auditState=-1):
        post_url = RequestUrl.search_batch_manage
        post_data = {
            "pageIndex": 1,
            "pageSize": 20,
            "auditState": auditState,
            "batchState": None,
            "batchCode": "",
            "productId": None,
            "product": ""
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('删除批号管理')
    def delete_batch_manage(self, id):
        post_url = RequestUrl.delete_batch_manage
        post_data = {
            "id": id
        }
        header = self.get_header()
        Request().postJson(post_url, post_data, header)

    @allure.step('获取当前班次信息')
    def get_current_schedule(self, workshop=1, time=datetime.now().strftime('%Y-%m-%d %H:%M:%S')):
        post_url = RequestUrl.get_current_schedule
        post_data = {
            "now": time,
            "workshopId": workshop
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']

    @allure.step('通过当前班次id获取值班人信息')
    def get_on_work_people_by_current_schedule(self, id):
        post_url = RequestUrl.list_with_replace_post
        post_data = {
            "scheduleId": id
        }
        header = self.get_header()
        r = Request().postJson(post_url, post_data, header)
        return r.json()['res']


if __name__ == '__main__':
    print(CommonFun().get_header())