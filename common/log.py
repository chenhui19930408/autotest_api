import time
import os
import logging.config
from dir_path import DirPath

"""日志配置"""
# 初始化logger对象
logger_name = "log"
logger = logging.getLogger(logger_name)
logger.setLevel(logging.DEBUG)
# 定义日志目录和日志文件名称
log_file_name = time.strftime("%Y_%m_%d %H_%M_%S")
log_file_path = DirPath.log_path
if not os.path.exists(log_file_path):
    os.mkdir(log_file_path)
log_name = log_file_path + "/" + log_file_name + '.log'
# 定义日志格式
fmt = '%(asctime)s [%(threadName)s] [%(name)s] [%(levelname)s] %(filename)s[line:%(lineno)d] %(message)s'
datefmt = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter(fmt, datefmt)
# 添加控制台输出和文件记录hanlder
fh = logging.FileHandler(log_name)
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)
logger.addHandler(fh)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)
