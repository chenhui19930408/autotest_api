from config import setting
from request.Request import Request
from request.RequestUrl import RequestUrl


class UserInfo:
    '''
    获取用户信息
    '''
    grant_code = ""
    access_token = ""
    client_id = setting.client_id
    user_id = ""

    def __init__(self):
        pass

    @classmethod
    def get_grant_code(cls):
        '''
        获取grantcode
        '''
        if cls.grant_code == "":
            post_url = RequestUrl.get_grant_code
            post_data = {
                "clientId": cls.client_id,
                "loginAccount": setting.login_user
            }
            r = Request().postJson(post_url, post_data)
            cls.grant_code = r.json()['result']['grantCode']
        return cls.grant_code

    @classmethod
    def login(cls):
        '''
        同时获取accessToken和userid
        '''
        post_url = RequestUrl.login
        post_data = {
                "grantCode": cls.get_grant_code(),
                "validateCode": setting.login_psw,
                "clientId": cls.client_id
                }
        header = {
            "corp-code": "9001",
            "orgCode": "9001"
        }
        r = Request().postJson(post_url, post_data, header=header)
        cls.user_id = r.json()['result']['userId']
        cls.access_token = r.json()['result']['accessToken']
        print('************************此次登录的token为******************：' + cls.access_token)
        print('************************此次登录的userid为******************：' + cls.user_id)
        return cls.access_token, cls.user_id

    @classmethod
    def get_access_token(cls):
        '''
        获取accesstoken
        '''
        if cls.access_token == "":
            cls.login()
        return cls.access_token

    @classmethod
    def get_user_id(cls):
        '''
        获取userid
        '''
        if cls.user_id == "":
            cls.login()
        return cls.user_id

    @classmethod
    def get_client_id(cls):
        return cls.client_id


if __name__ == "__main__":
    UserInfo.get_access_token()
