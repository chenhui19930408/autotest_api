import os

from locust import HttpUser

from testcase_locust.machine_manage.machine_record import MachineRecord
from testcase_locust.schedule_manage.schedule_manage import ScheduleManage


class WebsiteUser(HttpUser):
    tasks = [MachineRecord, ScheduleManage]
    host = 'http://qa-spinning.szzhijing.com'


if __name__ == "__main__":
    os.system("locust -f main_locust.py")


