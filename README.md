1、拉取项目以后设置读取的环境目录为项目中的venv目录，这里面已经安装好了需要用到的第三方包

common：存放通用方法
config：存放配置文件
logs：存放日志
reports：存放测试报告
request：存放封装Request类
testcase：存放用例集
utils：存放一些工具类