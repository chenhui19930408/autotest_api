import os

import pytest

from common.common_fun import CommonFun
from dir_path import DirPath
from request.Request import Request

if __name__ == '__main__':
    # module_list = ["base_data", "schedule_manage", 'user_manage', 'product_manage']
    # for module in module_list:
    #     pytest.main(['-vs', DirPath.testcase_path + '/' + module, '--alluredir', DirPath.report_source])
    # os.system('{} generate {} -o {} --clean'.format(DirPath.allure, DirPath.report_source, DirPath.report_html))

    url = 'https://qa-i.szzhijing.com/spinning-energy/energy/dailyTrend'
    data = {
        "workshopId": 18,
        "processId": 5
    }
    r = Request().postJson(url=url, json=data, header=CommonFun().get_header())
    e = r.json()['result']
    run = 0
    for i in range(len(e)):
        run = run + e[i]['totalEnergy']
    print(run)