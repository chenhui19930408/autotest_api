import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestMakeSchedule:

    def setup_class(self):
        # 获取工厂信息
        self.workshop = CommonFun().search_workshop()[0]
        self.workshop_id = self.workshop['id']
        self.workshop_name = self.workshop['name']
        # 获取班制信息
        self.shift_type = CommonFun().search_shift_type_by_name('两班制')
        self.shift_type_id = self.shift_type['id']
        self.shift_type_name = self.shift_type['name']
        # 获取班组信息
        self.group1 = CommonFun().search_group_team()[0]
        self.group2 = CommonFun().search_group_team()[1]
        self.group1_id = self.group1['id']
        self.group1_name = self.group1['name']
        self.group2_id = self.group2['id']
        self.group2_name = self.group2['name']
        # 获取班次信息
        self.shift1 = CommonFun().search_shift(type_id=self.shift_type_id)[0]
        self.shift2 = CommonFun().search_shift(type_id=self.shift_type_id)[1]
        self.shift1_id = self.shift1['id']
        self.shift1_name = self.shift1['name']
        self.shift2_id = self.shift2['id']
        self.shift2_name = self.shift2['name']

    def teardown_class(self):
        CommonFun().delete_daily_schedule('2021-07-13')
        CommonFun().delete_daily_schedule('2021-07-14')
        CommonFun().delete_daily_schedule('2021-07-15')

    @allure.story('生成排班')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/schedule_manage/schedule_manage/data/make_schedule.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$workshopId', str(self.workshop_id)).replace('$workshopName', self.workshop_name).\
            replace('$shiftTypeId', str(self.shift_type_id)).replace('$shiftTypeName', self.shift_type_name).\
            replace('$groupId1', str(self.group1_id)).replace('$groupName1', self.group1_name).\
            replace('$groupId2', str(self.group2_id)).replace('$groupName2', self.group2_name).\
            replace('$shiftId1', str(self.shift1_id)).replace('$shiftName1', self.shift1_name).\
            replace('$shiftId2', str(self.shift2_id)).replace('$shiftName2', self.shift2_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/schedule_manage/schedule_manage/test_make_schedule.py'])
