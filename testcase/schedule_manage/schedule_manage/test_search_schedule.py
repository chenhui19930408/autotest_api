import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestSearchSchedule:

    def setup_class(self):
        # 获取工厂信息
        self.workshop = CommonFun().search_workshop()[0]
        self.workshop_id = self.workshop['id']

    @allure.story('查询排班日历')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/schedule_manage/schedule_manage/data/search_schedule.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$workshopId', str(self.workshop_id))
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/schedule_manage/schedule_manage/test_search_schedule.py'])
