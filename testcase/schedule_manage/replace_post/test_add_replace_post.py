import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddReplacePost:

    def setup_class(self):
        self.ids = []
        self.replaceTime = '2021-07-07 15:23:02'
        # 获取工厂信息
        self.workshop = CommonFun().search_workshop()[0]
        self.workshop_id = self.workshop['id']
        self.workshop_name = self.workshop['name']
        # 获取当前班次信息
        self.current_schedule = CommonFun().get_current_schedule(workshop=self.workshop_id, time=self.replaceTime)
        self.shift_name = self.current_schedule['shiftName']
        self.schedule_id = self.current_schedule['id']
        self.shift_id = self.current_schedule['shiftId']
        self.shift_type_name = self.current_schedule['shiftTypeName']
        self.shift_type_id = self.current_schedule['shiftTypeId']
        self.belong_date = self.current_schedule['belongDate']
        self.begin_time = self.current_schedule['beginTime']
        self.end_time = self.current_schedule['endTime']
        # 获取原值班人信息
        self.old_people = CommonFun().get_on_work_people_by_current_schedule(self.schedule_id)[0]
        self.old_emp_id = self.old_people['empId']
        self.old_emp_name = self.old_people['empName']
        self.group_id = self.old_people['groupId']
        self.group_name = self.old_people['groupName']
        self.post_id = self.old_people['scheduleEmpPostList'][0]['postId']
        self.post_name = self.old_people['scheduleEmpPostList'][0]['postName']
        self.process_id = self.old_people['scheduleEmpPostList'][0]['processId']
        self.process_code = self.old_people['scheduleEmpPostList'][0]['processCode']
        self.process_name = self.old_people['scheduleEmpPostList'][0]['processName']
        self.schedule_emp_id = self.old_people['id']
        # 获取新值班人信息
        self.new_people = CommonFun().get_user_infos(workshop=self.workshop_id)[0]
        self.new_emp_id = self.new_people['id']
        self.new_emp_name = self.new_people['name']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_replace_post(self.ids[i])

    @allure.story('新增人员替岗')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/schedule_manage/replace_post/data/add_replace_post.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$workshopId', str(self.workshop_id)).replace('$workshopName', self.workshop_name).\
            replace('$shiftName', self.shift_name).replace('$scheduleId', str(self.schedule_id)).\
            replace('$oldEmpId', str(self.old_emp_id)).replace('$oldEmpName', self.old_emp_name).\
            replace('$groupId', str(self.group_id)).replace('$groupName', self.group_name).\
            replace('$postId', str(self.post_id)).replace('$newEmpId', str(self.new_emp_id)).\
            replace('$newEmpName', self.new_emp_name).replace('$shiftId', str(self.shift_id)).\
            replace('$shiftTypeId', str(self.shift_type_id)).replace('$shiftTypeName', self.shift_type_name).\
            replace('$postName', self.post_name).replace('$processCode', self.process_code).\
            replace('$processId', str(self.process_id)).replace('$processName', self.process_name).\
            replace('$belongDate', self.belong_date).replace('$beginTime', self.begin_time).\
            replace('$endTime', self.end_time).replace('$scheduleEmpId', str(self.schedule_emp_id)).\
            replace('$replaceTime', self.replaceTime)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/schedule_manage/replace_post/test_add_replace_post.py'])
