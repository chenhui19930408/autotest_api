import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestApproveUnapproveClassSystem:

    def setup_class(self):
        self.id = CommonFun().add_shift_and_get_id()

    def teardown_class(self):
        CommonFun().delete_shift(self.id)

    @allure.story('审核反审核班制管理')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/schedule_manage/shift_manage/data/approve_unapprove_shift.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$id', str(self.id))
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/schedule_manage/shift_manage/test_apprve_unapprove_shift.py'])
