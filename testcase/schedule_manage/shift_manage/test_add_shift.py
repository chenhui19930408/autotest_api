import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddClassSystem:

    def setup_class(self):
        self.ids = []
        self.shift_type = CommonFun().search_shift_type_by_name('两班制')
        self.shift_type_id = self.shift_type['id']
        self.shift_type_name = self.shift_type['name']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_shift(self.ids[i])

    @allure.story('新增班制')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/schedule_manage/shift_manage/data/add_shift.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$typeId', str(self.shift_type_id)).replace('$typeName', self.shift_type_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/schedule_manage/shift_manage/test_add_shift.py'])
