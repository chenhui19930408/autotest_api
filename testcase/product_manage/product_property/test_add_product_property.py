import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddProductType:

    def setup_class(self):
        self.ids = []
        # 获取品种属性字典
        self.type = CommonFun().search_dict('property_type')[0]
        self.type_id = self.type['id']
        self.type_name = self.type['name']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_product_property(self.ids[i])

    @allure.story('新增物料属性')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/product_manage/product_property/data/add_product_property.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$typeId', str(self.type_id)).replace('$typeName', self.type_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/product_manage/product_property/test_add_product_property.py'])
