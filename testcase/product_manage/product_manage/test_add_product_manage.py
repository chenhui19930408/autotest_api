import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddProductManage:

    def setup_class(self):
        self.ids = []
        # 获取物料分类
        self.product_type = CommonFun().search_product_type(parent_id=1)[1]
        self.product_id = self.product_type['id']
        self.product_name = self.product_type['name']
        self.product_code = self.product_type['code']
        self.product_type_id = self.product_type['typeId']
        self.product_type_name = self.product_type['typeName']
        # 获取基准计量单位
        self.unit = CommonFun().search_unit()[0]
        self.unit_id = self.product_type['id']
        self.unit_name = self.product_type['name']
        self.unit_code = self.product_type['code']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_product_manage(self.ids[i])

    @allure.story('新增物料档案')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/product_manage/product_manage/data/add_product_manage.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$categoryId', str(self.product_id)).replace('$categoryCode', self.product_code).\
            replace('$categoryName', self.product_name).replace('$typeId', str(self.product_type_id)).\
            replace('$typeName', self.product_type_name).replace('$unitId', str(self.unit_id)).\
            replace('$unitName', self.unit_name).replace('$unitCode', self.unit_code)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/product_manage/product_manage/test_add_product_manage.py'])
