import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddProductType:

    def setup_class(self):
        self.ids = []
        # 获取存货类型信息
        self.type = CommonFun().search_dict('stock_type')[0]
        self.type_id = self.type['id']
        self.type_name = self.type['name']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_product_type(self.ids[i])

    @allure.story('新增物料分类')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/product_manage/product_type/data/add_product_type.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$typeId', str(self.type_id)).replace('$typeName', self.type_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/product_manage/product_type/test_add_product_type.py'])
