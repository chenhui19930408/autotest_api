import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddBatchManage:

    def setup_class(self):
        self.ids = []
        # 获取物料档案
        self.product_manage = CommonFun().search_product_manage()[0]
        self.product_id = self.product_manage['id']
        self.product_name = self.product_manage['name']
        self.product_code = self.product_manage['code']
        self.product_models = self.product_manage['models']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_batch_manage(self.ids[i])

    @allure.story('新增物料档案')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/product_manage/batch_manage/data/add_batch_manage.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$productId', str(self.product_id)).replace('$productName', self.product_name).\
            replace('$productCode', self.product_code).replace('$productModels', str(self.product_models))
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/product_manage/batch_manage/test_add_batch_manage.py'])
