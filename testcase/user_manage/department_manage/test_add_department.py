import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddDepartment:

    def setup_class(self):
        self.ids = []
        # 获取员工信息
        self.user = CommonFun().get_user_infos()[0]
        self.user_id = self.user['id']
        self.user_name = self.user['name']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_department(self.ids[i])

    @allure.story('新增部门')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/user_manage/department_manage/data/add_department.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$leaderId', str(self.user_id)).replace('$leaderName', self.user_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/user_manage/department_manage/test_add_department.py'])
