import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestQuitEmployee:

    def setup_class(self):
        # 新增员工
        self.id = CommonFun().add_user_and_get_id('自动化测试数据', '012345')

    def teardown_class(self):
        # 删除员工
        CommonFun().delete_user(self.id)

    @allure.story('员工离职')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/user_manage/employee_manage/data/quit_employee.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$id', str(self.id))
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/user_manage/employee_manage/test_quit_employee.py'])
