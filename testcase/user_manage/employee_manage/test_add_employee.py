import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddEmployee:

    def setup_class(self):
        self.ids = []
        # 获取部门信息
        self.dept = CommonFun().search_department()[0]
        self.deptId = self.dept['id']
        self.deptName = self.dept['name']
        # 获取岗位信息
        self.post = CommonFun().search_post_infos()[0]
        self.postId = self.post['id']
        self.postName = self.post['name']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_user(self.ids[i])

    @allure.story('新增员工')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/user_manage/employee_manage/data/add_employee.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$deptId', str(self.deptId)).replace('$deptName', self.deptName).\
            replace('$postId', str(self.postId)).replace('$postName', self.postName)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/user_manage/employee_manage/test_add_employee.py'])
