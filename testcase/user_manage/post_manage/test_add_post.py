import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddPost:

    def setup_class(self):
        self.ids = []
        # 获取岗位信息
        self.post_type = CommonFun().search_dict('post_type')[0]
        self.type_id = self.post_type['id']
        self.type_name = self.post_type['name']
        # 获取工序信息
        self.process = CommonFun().get_process_infos()[0]
        self.process_id = self.process['id']
        self.process_name = self.process['name']
        self.process_code = self.process['code']

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_post(self.ids[i])

    @allure.story('新增岗位')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/user_manage/post_manage/data/add_post.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$typeId', str(self.type_id)).replace('$typeName', self.type_name).\
            replace('$processId', str(self.process_id)).replace('$processName', self.process_name).\
            replace('$processCode', self.process_code)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/user_manage/post_manage/test_add_post.py'])
