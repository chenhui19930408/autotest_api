import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddColor:

    def setup_class(self):
        self.ids = []

    def teardown_class(self):
        for i in range(len(self.ids)):
            CommonFun().delete_color(self.ids[i])

    @allure.story('添加管圈颜色')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/base_data/tubo_color/data/add_color.xls').getTestData())
    def test_01(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            self.ids.append(res.json()['res'])
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/base_data/tubo_color/test_add_color.py'])
