import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddWorkshop:

    id = ''

    def teardown_class(self):
        CommonFun().delete_workshop(id)

    @allure.story('添加工厂')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/base_data/workshop_manage/data/add_worshop.xls').getTestData())
    def test_01_add_workshop(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            global id
            id = res.json()['res']
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/base_data/workshop_manage/test_add_workshop.py'])
