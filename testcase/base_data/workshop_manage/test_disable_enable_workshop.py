import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestDisableEnableWorkshop:

    def setup_class(self):
        self.id = CommonFun().add_workshop_and_get_id('自动化测试数据', '1234')

    def teardown_class(self):
        CommonFun().delete_workshop(self.id)

    @allure.story('停用启用工厂')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/base_data/workshop_manage/data/disable_enable_worshop.xls').getTestData())
    def test_01_disable_enable_workshop(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        post_data = post_data.replace('$id', str(self.id))
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/base_data/workshop_manage/test_disable_enable_workshop.py'])
