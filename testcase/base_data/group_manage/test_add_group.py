import pytest

from common.assert_unil import AssertUtils
from common.common_fun import CommonFun
from utils import *
from dir_path import DirPath
from request.Request import Request


class TestAddGroup:

    id = ''

    def teardown_class(self):
        CommonFun().delete_group_team(id)

    @allure.story('添加班组')
    @pytest.mark.parametrize(*ExcelUtils(DirPath.testcase_path + '/base_data/group_manage/data/add_group.xls').getTestData())
    def test_01_edit_dict(self, case_name, post_url, post_data, assert_text, assert_json):
        allure.dynamic.title(case_name)
        res = Request().postJson(url=post_url, json=eval(post_data), header=CommonFun().get_header())
        if res.json()['status'] == 200:
            global id
            id = res.json()['res']
        assert AssertUtils().assert_text(res.text, assert_text)
        assert AssertUtils().assert_json(res.json(), assert_json)


if __name__ == "__main__":
    pytest.main(['-vs', DirPath.testcase_path + '/base_data/group_manage/test_add_group.py'])
