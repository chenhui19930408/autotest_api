from config import setting


class RequestUrl:
    # 获取grantcode
    get_grant_code = setting.get_grant_api
    # 服务名
    staff = '/spinning-staff'
    base = '/spinning-base'
    schedule = '/spinning-schedule'
    # 登录
    login = setting.host_api + staff + '/base/auth/login'

    # 获取当前用户信息
    get_current_user_data = setting.host_api + '/userData/workshops'
    # 新增字典管理
    add_dict = setting.host_api + '/frame/dict/save'
    # 查询字典管理
    search_dict = setting.host_api + '/frame/dict/list'
    # 删除字段管理
    delete_dict = setting.host_api + '/frame/dict/delete'
    # 查询计量单位
    search_unit = setting.host_api + '/unit/list'
    # 新增车间
    add_workshop = setting.host_api + '/workshop/save'
    # 查询车间
    search_workshop = setting.host_api + '/workshop/list'
    # 删除车间
    delete_workshop = setting.host_api + '/workshop/delete'
    # 添加班组
    add_group_team = setting.host_api + '/group/save'
    # 查询班组
    search_group_team = setting.host_api + '/group/list'
    # 删除班组
    delete_group_team = setting.host_api + '/group/delete'
    # 新增管圈颜色
    add_color = setting.host_api + '/tubeColor/save'
    # 删除管圈颜色
    delete_color = setting.host_api + '/tubeColor/delete'
    # 新增班制
    add_shift = setting.host_api + '/shift/save'
    # 查询班制管理
    search_shift = setting.host_api + '/shift/list'
    # 删除班制
    delete_shift = setting.host_api + '/shift/delete'
    # 新增排班
    make_schedule = setting.host_api + '/schedule/saveWrap'
    # 删除人员替岗
    delete_replace_post = setting.host_api + '/scheduleReplacePost/deleteBatch'
    # 添加/修改生产设备
    add_update_equipment = setting.host_api + '/machine/save'
    # 查询生产设备
    search_equipment = setting.host_api + '/machine/list'
    # 删除设备
    delete_equipment = setting.host_api + '/machine/delete'
    # 新增员工
    add_user = setting.host_api + '/emp/saveWrap'
    # 查询员工
    emp_list = setting.host_api + '/emp/list'
    # 删除员工
    delete_user = setting.host_api + '/emp/delete'
    # 获取工序
    get_process = setting.host_api + '/process/list'
    # 新增岗位
    add_post = setting.host_api + '/post/save'
    # 查询岗位
    search_post = setting.host_api + '/post/list'
    # 删除岗位
    delete_post = setting.host_api + '/post/deleteBatch'
    # 新增部门
    add_department = setting.host_api + '/dept/save'
    # 查询部门
    search_department = setting.host_api + '/dept/list'
    # 删除部门
    delete_department = setting.host_api + '/dept/delete'
    # 新增物料分类
    add_product_type = setting.host_api + '/productCategory/save'
    # 查询物料分类
    search_product_type = setting.host_api + '/productCategory/list'
    # 删除物料分类
    delete_product_type = setting.host_api + '/productCategory/delete'
    # 新增物料属性
    add_product_property = setting.host_api + '/productProperty/save'
    # 查询物料属性
    search_product_property = setting.host_api + '/productProperty/list'
    # 删除物料属性
    delete_product_property = setting.host_api + '/productProperty/delete'
    # 新增物料档案
    add_product_manage = setting.host_api + '/product/saveWrap'
    # 查询物料档案
    search_product_manage = setting.host_api + '/product/list'
    # 删除物料档案
    delete_product_manage = setting.host_api + '/product/delete'
    # 新增批号管理
    add_batch_manage = setting.host_api + '/productBatch/save'
    # 查询批号管理
    search_batch_manage = setting.host_api + '/productBatch/list'
    # 删除批号管理
    delete_batch_manage = setting.host_api + '/productBatch/delete'
    # 获取当前班次信息
    get_current_schedule = setting.host_api + '/schedule/currentSchedule'
    # 获取原值班人信息
    list_with_replace_post = setting.host_api + '/scheduleEmp/listWithReplacePost'

    # 设备列表
    machine_page = setting.host_api + base + '/machine/page'
    # 车间列表
    workshop_page = setting.host_api + base + '/workshop/page'
    # 排班域
    # 获取排班列表
    schedule_calendar = setting.host_api + schedule + '/schedule/calendar'
