import os
import time


class DirPath:
    # 获取当前时间
    now_time = time.strftime('%Y-%m-%d_%H_%M_%S')
    # 获取根目录
    rootPath = os.path.dirname(__file__)
    # allure的启动目录
    allure = rootPath + '/common/allure/bin/allure.bat'
    # report的资源目录
    report_source = rootPath + '/reports/' + now_time + '/result'
    # report的生成报告目录
    report_html = rootPath + '/reports/' + now_time + '/report'
    # testcase的路径
    testcase_path = rootPath + '/testcase'
    # 存放日志的目录
    log_path = rootPath + '/logs'
    # 配置文件目录
    config_path = rootPath + '/config'
    # mqtt发送文件的excel路径
    mqtt_data_xs_path = rootPath + '/tools/mqtt_utils/data_xs.xlsx'
    # mqtt发送文件的excel路径
    mqtt_data_lt_path = rootPath + '/tools/mqtt_utils/data_lt.xlsx'
    # locust的文件
    locust_path = rootPath + '/testcase_locust/test.py'


if __name__ == "__main__":
    print(DirPath.rootPath)
    print(DirPath.report_source)
    print(DirPath.report_html)
    print(DirPath.allure)
