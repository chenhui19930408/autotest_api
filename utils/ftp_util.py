from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer


class FtpUtils:

    def __init__(self, user, password, path):
        # 实例化DummyAuthorizer来创建ftp用户
        self.authorizer = DummyAuthorizer()
        # 参数：用户名，密码，目录，权限
        self.authorizer.add_user(user, password, path, perm='elradfmwMT')
        self.handler = FTPHandler
        self.handler.authorizer = self.authorizer

    def startServer(self, host, port):
        print('启动ftp服务：' + str(host) + ':' + str(port))
        server = FTPServer((host, port), self.handler)
        server.serve_forever()


if __name__ == '__main__':
    FtpUtils('admin', '123', r'E:/autotest_api').startServer('127.0.0.1', 2122)
