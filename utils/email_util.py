from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

import allure

from config import setting
from dir_path import DirPath


class EmailUtils:

    def __init__(self, smtpserver=setting.email_smtp_server, username=setting.email_from_address,
                 password=setting.email_password, receiver=setting.email_to_address, title='接口自动化邮件提醒',
                 content='接口自动化测试邮件内容', file=None):
        self.smtpserver = smtpserver
        self.username = username
        self.password = password
        self.receiver = receiver
        self.title = title
        self.content = content
        self.file = file

    @allure.step('发送邮件')
    def send_mail(self):
        # MIME
        msg = MIMEMultipart()
        # 初始化邮件信息
        msg.attach(MIMEText(self.content, _charset="utf-8"))
        msg["Subject"] = self.title
        msg["From"] = self.username
        msg["To"] = self.receiver
        # 邮件附件
        # 判断是否附件
        if self.file:
            # MIMEText读取文件
            att = MIMEText(open(self.file).read())
            # 设置内容类型
            att["Content-Type"] = 'application/octet-stream'
            # 设置附件头
            att["Content-Disposition"] = 'attachment;filename="%s"' % self.file
            # 将内容附加到邮件主体中
            msg.attach(att)
        # 登录邮件服务器
        self.smtp = smtplib.SMTP(self.smtpserver, port=25)
        self.smtp.login(self.username, self.password)
        # 发送邮件
        self.smtp.sendmail(self.username, self.receiver, msg.as_string())


if __name__ == "__main__":
    file = DirPath.rootPath + "/reports/2021-06-23_14_52_46/report/index.html"
    email = EmailUtils(file=file)
    email.send_mail()



