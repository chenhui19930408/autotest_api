import allure
import pymysql

from config import setting


class DataBaseUtils:

    def __init__(self, host=setting.host, port=setting.port, username=setting.username, password=setting.password, basename=setting.basename):
        self.db = pymysql.connect(host=host, port=port, user=username, password=password,
                                  database=basename, charset='utf8')

    def execute(self, sql):
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor

    @allure.step('检索数据库')
    def select(self, sql):
        cur = self.execute(sql)
        self.db.close()
        return cur.fetchall()

    @allure.step('新增数据库')
    def insert(self, sql):
        self.execute(sql)
        self.db.commit()
        self.db.close()

    @allure.step('删除数据库')
    def delete(self, sql):
        self.execute(sql)
        self.db.commit()
        self.db.close()

    def select_and_get_count(self, sql):
        return len(self.select(sql))


if __name__ == '__main__':
    print(DataBaseUtils().select_and_get_count('select * from base_equipment'))