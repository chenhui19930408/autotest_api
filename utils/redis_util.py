import json

import redis

from config import setting


class RedisUtils:

    key = ''
    hash_key = ''

    def __init__(self, host=setting.redis_host, port=setting.redis_port, password=setting.redis_password):
        pool = redis.ConnectionPool(host=host, port=port, password=password)
        self.r = redis.Redis(connection_pool=pool)

    def set_key(self, key):
        self.key = key
        return self

    def set_hashkey(self, hash_key):
        self.hash_key = hash_key
        return self

    def send(self, msg):
        if self.key == '':
            print('未设置key， 无法新增redis')
            return
        self.r.hset(self.key, self.hash_key, json.dumps(msg))
        print('新增redis成功，key：' + self.key)

    def delete_by_key(self, key):
        self.r.delete(key)
        print('删除redis成功，key：' + key)
