import os


class YamlUtils:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        '''
        获取指定目录下所有yaml文件
        '''
        list = []
        for catalogueName, catalogueList, fileList in os.walk(top=self.path, topdown=True):
            for file in fileList:
                if '.yaml' in file:
                    print(catalogueName+'\\'+file)
                    list.append(catalogueName+'\\'+file)
        return list
