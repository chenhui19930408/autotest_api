import json

import xlrd

from common.log import logger
from config import setting
from dir_path import DirPath


class ExcelUtils:

    book = None
    sheet = None
    book_name = ''
    totle_row = 0
    totle_col = 0

    def __init__(self, filepath, sheet_name=None):
        filename = filepath.split('/')
        self.book_name = filename[len(filename) - 1]
        self.book = xlrd.open_workbook(filepath)
        if sheet_name:
            self.sheet = self.book.sheet_by_name(sheet_name)
        else:
            self.sheet = self.book.sheet_by_index(0)
        self.totle_row = self.sheet.nrows
        self.totle_col = self.sheet.ncols

    def readByCell(self, row, col):
        logger.info('read excel(' + str(self.book_name) + ').sheet(' + str(self.sheet.name) + ').cell(' + str(row) + ',' + str(col) + ')')
        return self.sheet.cell(row, col).value

    def excelToJson(self, row_no=None):
        '''
        row_no: 为空则格式化全部，否则格式化指定行
        '''
        if row_no:
            d = {}
            for j in range(0, self.totle_col):
                key = '%s' % self.sheet.cell(0, j).value
                d[key] = self.sheet.cell(row_no, j).value
            ap = []
            for k, v in d.items():
                if isinstance(v, float):  # excel中的值默认是float,需要进行判断处理，通过'"%s":%d'，'"%s":"%s"'格式化数组
                    ap.append('"%s":%d' % (k, v))
                else:
                    if v == 'None':
                        ap.append('"%s":"%s"' % (k, v))
                    else:
                        ap.append('"%s":"%s"' % (k, v))
            s = '{%s}' % (','.join(ap))  # 继续格式化
            print(s)
            return s
        else:
            p = []
            for i in range(2, self.totle_row):
                d = {}
                for j in range(0, self.totle_col):
                    q = '%s' % self.sheet.cell(0, j).value
                    d[q] = self.sheet.cell(i, j).value
                ap = []
                for k, v in d.items():
                    if isinstance(v, float):  # excel中的值默认是float,需要进行判断处理，通过'"%s":%d'，'"%s":"%s"'格式化数组
                        if k=='f70' or k=='f71':
                            ap.append('"%s":%f' % (k, v))
                        else:
                            ap.append('"%s":%d' % (k, v))
                    else:
                        try:
                            if isinstance(json.loads(v), dict):
                                ap.append('"%s":%s' % (k, v))
                            elif v == 'None':
                                ap.append('"%s":"%s"' % (k, v))
                            else:
                                ap.append('"%s":"%s"' % (k, v))
                        except:
                            if v == 'None':
                                ap.append('"%s":"%s"' % (k, v))
                            else:
                                ap.append('"%s":"%s"' % (k, v))
                s = '{%s}' % (','.join(ap))  # 继续格式化
                p.append(s)
            t = '[%s]' % (','.join(p))  # 格式化
            print(t)
            return t

    def getTestData(self):
        list = self.readData()
        param_name = ','.join(list[0])
        data = list[1:]
        return param_name, data

    def readData(self):
        list = []
        list.append([])
        url_col = 0
        for col in range(self.totle_col):
            if self.sheet.cell(0, col).value == 'post_url':
                url_col = col
            list[0].append(self.sheet.cell(0, col).value)
        for row in range(self.totle_row - 1):
            list.append([])
            row = row + 1
            for col1 in range(self.totle_col):
                if col1 == url_col:
                    value = setting.host_api + self.sheet.cell(row, col1).value
                else:
                    value = self.sheet.cell(row, col1).value
                list[row].append(value)
        return list


if __name__ == "__main__":
    ExcelUtils(DirPath.mqtt_data_path, 'data').excelToJson(10)
