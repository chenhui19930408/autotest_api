import json

from locust.user import TaskSet, task

from common.common_fun import CommonFun
from request.RequestUrl import RequestUrl


class ScheduleManage(TaskSet):

    @task(1)
    def schedule_calendar(self):
        url = RequestUrl.schedule_calendar
        data = {
            "workshopId": 18,
            "beginDate": "2021-11-29",
            "endDate": "2022-01-09"
        }
        r = self.client.post(url=url, data=json.dumps(data), headers=CommonFun().get_header())
        print(r.text)
