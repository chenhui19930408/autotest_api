import json

from locust.user import TaskSet, task

from common.common_fun import CommonFun
from request.RequestUrl import RequestUrl


class MachineRecord(TaskSet):

    @task(1)
    def machine_page(self):
        url = RequestUrl.machine_page
        data = {
            "pageNo": 1,
            "pageSize": 100,
            "workshopId": 18,
            "processId": 5,
            "typeId": "",
            "orValue": ""
        }
        r = self.client.post(url=url, data=json.dumps(data), headers=CommonFun().get_header())
        print(r.text)
