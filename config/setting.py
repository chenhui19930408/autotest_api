import configparser

from dir_path import DirPath


# 读取配置文件
cf = configparser.ConfigParser()
cf.read(DirPath.config_path + "/config.ini", encoding='utf-8')
# 读取配置文件中的变量
environment = cf.get("evn", "environment")
if environment == 'dev':
    host_api = cf.get("url", "dev_host_api")
    get_grant_api = cf.get("url", "dev_get_grant_api")
    collector_bind_in_gemi_api = cf.get("url", "dev_collector_bind_in_gemi_api")

    login_user = cf.get("user", "dev_login_user")
    login_psw = cf.get("user", "dev_login_psw")
    client_id = cf.get("user", "dev_client_id")

    host = cf.get("database", "dev_db_host")
    port = cf.getint("database", "dev_db_port")
    username = cf.get("database", "dev_db_username")
    password = cf.get("database", "dev_db_password")
    basename = cf.get("database", "dev_db_basename")
elif environment == 'test':
    host_api = cf.get("url", "test_host_api")
    get_grant_api = cf.get("url", "test_get_grant_api")
    collector_bind_in_gemi_api = cf.get("url", "test_collector_bind_in_gemi_api")

    login_user = cf.get("user", "test_login_user")
    login_psw = cf.get("user", "test_login_psw")
    client_id = cf.get("user", "test_client_id")

    host = cf.get("database", "test_db_host")
    port = cf.getint("database", "test_db_port")
    username = cf.get("database", "test_db_username")
    password = cf.get("database", "test_db_password")
    basename = cf.get("database", "test_db_basename")

db_handle_flag = cf.get("database", "db_handle_flag")
# 导入email数据
email_from_address = cf.get("email", "email_from_address")
email_password = cf.get("email", "email_password")
email_smtp_server = cf.get("email", "email_smtp_server")
email_to_address = cf.get("email", "email_to_address")
# 导入mqtt数据
mqtt_host = cf.get("mqtt", "host")
mqtt_port = cf.getint("mqtt", "port")
mqtt_user = cf.get("mqtt", "user")
mqtt_password = cf.get("mqtt", "password")
mqtt_topic = cf.get("mqtt", "topic")
# redis配置
redis_host = cf.get("redis", "host")
redis_port = cf.get("redis", "port")
redis_password = cf.get("redis", "password")
