from datetime import datetime, timedelta
from time import sleep

from request.Request import Request
from utils import DataBaseUtils

output = 0


def get_db_data_by_time(time_from, time_to):
    print("检索数据库-开始日期:  " + time_from + ", 结束日期:  " + time_to)
    db_data = DataBaseUtils(basename='spinningmes_collect').select('SELECT * FROM xs WHERE create_time BETWEEN "'+time_from+'" AND "'+time_to+'"')
    return db_data


def post_iot_data():
    start_time = datetime(2021, 7, 27, 21, 57, 46)
    end_time = (start_time + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
    while True:
        data_list = get_db_data_by_time(str(start_time), str(end_time))
        if len(data_list) == 0:
            break
        else:
            for i in range(len(data_list)):
                sleep(0.5)
                print("发送: "+str(i)+"/"+str(len(data_list)))
                post_data(db_to_json(data_list[i]))
                global output
                output = output + 1
        start_time = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S")
        end_time = (start_time + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')


def post_data(data):
    post_url = "https://qa-zthh-spinning.szzhijing.com/spinning_zhongtai_back_collector/collector/xs"
    post_data = data
    header = {
        'auth-token': '11ad36610635484395f8e78ac29d5a0e',
        'corp-code': '9001',
        'Content-Type': 'application/json'
    }
    print("发送数据:  " + str(post_data))
    r = Request().postJson(post_url, post_data, header)
    if not r.json()['message'] == 'success':
        print('发送失败')


def db_to_json(db_data):
    global output
    json = {
        "body": [
            {
                "workshopId": str(db_data[1]),
                "machineCode": "XS007",
                "gatherTime": str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
                "spindleSpeed": int(db_data[7]) if db_data[7] else None,
                "frontRollerSpeed": int(db_data[8]) if db_data[8] else None,
                "draftMultiple": float(db_data[9]) if db_data[9] else None,
                "fixedLength": int(db_data[10]) if db_data[10] else None,
                "currentLength": int(db_data[11]) if db_data[11] else None,
                "twist": int(db_data[12]) if db_data[12] else None,
                # "output": float(db_data[15]) if db_data[15] else None,
                "output": 1000,
                "tex": int(db_data[13]) if db_data[13] else None,
                "frontRollerDiameter": int(db_data[27]) if db_data[27] else None,
                "shutdown": int(db_data[44]) if db_data[44] else None,
                # "ia": float(db_data[16]) if db_data[16] else None,
                "ia": 10,
                "tpf": float(db_data[20]) if db_data[20] else None,
                "uab": float(db_data[21]) if db_data[21] else None,
                # "tae": float(db_data[24]) if db_data[24] else None,
                "tae": 1000,
                "temp": int(db_data[31]) if db_data[27] else None
            }
        ],
        "corpCode": "9001"
    }
    return json


if __name__ == "__main__":
    post_iot_data()