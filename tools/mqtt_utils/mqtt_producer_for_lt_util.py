import json
import math
import time
import paho.mqtt.publish as publish
from config import setting
from dir_path import DirPath
from utils import ExcelUtils
from utils.redis_util import RedisUtils


class Send:

    host = setting.mqtt_host
    port = setting.mqtt_port
    user = setting.mqtt_user
    pwd = setting.mqtt_password
    topic = setting.mqtt_topic

    def publish(self, data_list):
        # 清空redis数据
        RedisUtils().delete_by_key('SPINNING:LT:9001:' + data_list[0]['ltname'])
        for i in range(len(data_list)):
            data = data_list[i]
            # 修改json内容
            lt_name = data['ltname']
            del data['ltname']
            # 封装请求体
            msg = {
                "a": "LT",
                "d": {
                    "children": {
                        lt_name: data
                    },
                    "ctime": int(time.time()),
                    "dataSource": "winder",
                    "rtime": int(time.time())
                }
            }
            # 发送请求
            publish.single(topic=self.topic, payload=json.dumps(msg), qos=1, client_id=str(math.floor(time.time())),
                           hostname=self.host, retain=False, auth={'username': self.user, 'password': self.pwd})
            print('发送成功: ' + str(msg))
            time.sleep(2)


if __name__ == '__main__':
    post_data_list = json.loads(ExcelUtils(DirPath.mqtt_data_path, sheet_name='lt_data').excelToJson())
    Send().publish(post_data_list)
