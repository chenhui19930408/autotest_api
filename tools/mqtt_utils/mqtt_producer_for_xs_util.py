import collections
import json
import math
import threading
import time
from datetime import datetime, timedelta

from paho.mqtt import client as mqtt_client
import paho.mqtt.publish as publish
from config import setting
from dir_path import DirPath
from utils import ExcelUtils

dict_time = {}  # 时间
sleep = 5


class Thread(threading.Thread):

    def __init__(self, host=setting.mqtt_host, port=setting.mqtt_port, user=setting.mqtt_user,
                 pwd=setting.mqtt_password, topic=setting.mqtt_topic, date='设备状态监控'):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.user = user
        self.pwd = pwd
        self.topic = topic
        self.date = date

    def run(self):
        post_data_list = json.loads(ExcelUtils(DirPath.mqtt_data_xs_path, sheet_name=self.date).excelToJson())
        Long_Send(self.host, self.port, self.user, self.pwd, self.topic).publish(post_data_list)
        # Short_Send(self.host, self.port, self.user, self.pwd, self.topic).publish(post_data_list)


class Short_Send:

    def __init__(self, host=setting.mqtt_host, port=setting.mqtt_port, user=setting.mqtt_user,
                 pwd=setting.mqtt_password, topic=setting.mqtt_topic):
        self.host = host
        self.port = port
        self.user = user
        self.pwd = pwd
        self.topic = topic

    def publish(self, data_list):
        for i in range(len(data_list)):
            # 处理数据
            data = data_list[i]
            data = deal_data(data)
            # 合并请求体
            msg = {
                "a": "jkn128898987t",
                "d": data
            }
            # 发送请求
            publish.single(topic=self.topic, payload=json.dumps(msg), qos=1, client_id=str(math.floor(time.time())),
                           hostname=self.host, retain=False, auth={'username': self.user, 'password': self.pwd})
            print('发送成功: ' + str(msg))
            time.sleep(sleep)


class Long_Send:

    send_data_list = ''

    def __init__(self, host=setting.mqtt_host, port=setting.mqtt_port, user=setting.mqtt_user,
                 pwd=setting.mqtt_password, topic=setting.mqtt_topic):
        self.host = host
        self.port = port
        self.user = user
        self.pwd = pwd
        self.topic = topic

    def do_publish(self, client):
        message = client._userdata.popleft()
        payload = message['payload']
        payload = deal_data(payload)
        # 合并请求体
        msg = {
            "a": "jkn128898987t",
            "d": payload
        }
        post_data = json.dumps(msg)
        message['payload'] = post_data
        # 发送请求
        if isinstance(message, dict):
            client.publish(**message)
            print(str(self.topic) + '发送成功: ' + str(msg))
        elif isinstance(msg, (tuple, list)):
            client.publish(self.topic, msg)
            print(str(self.topic) + '发送成功: ' + str(msg))
        else:
            raise TypeError('message must be a dict, tuple, or list')
        time.sleep(sleep)

    def on_publish(self, client, userdata, mid):
        if len(userdata) == 0:
            client.disconnect()
            print(str(self.topic) + '设备mqtt断开连接')
        else:
            self.do_publish(client)

    def on_connect(self, client, userdata, flags, rc):
        print(client, userdata, flags, rc)
        if rc == 0:
            print(str(self.topic) + '设备mqtt服务器连接成功')
            if len(userdata) > 0:
                self.do_publish(client)
            else:
                print('发送失败：userdata为空')
        else:
            print('发送失败：连接rc失败')

    def connect_mqtt(self):
        msgs = []
        for i in range(len(self.send_data_list)):
            msg = {'topic': self.topic, 'payload': self.send_data_list[i], 'qos': 1, 'retain': False}
            msgs.append(msg)
        client = mqtt_client.Client(userdata=collections.deque(msgs))
        client.on_connect = self.on_connect
        client.on_publish = self.on_publish
        client.username_pw_set(self.user, self.pwd)
        client.connect(self.host, self.port, keepalive=60)
        client.loop_forever()

    def publish(self, data_list):
        self.send_data_list = data_list
        self.connect_mqtt()


def deal_data(data):
    data['rtime'] = int(time.time())  # 数据上报时间
    data['ctime'] = int(time.time())  # 数据上报时间
    if data['mt'] == 1 and data['ec'] == 1401:
        if data['f63'] != '' and data['f63'] not in dict_time:
            dict_time[data['f63']] = int(time.time())
        if data['f81'] != '' and data['f81'] not in dict_time:
            dict_time[data['f81']] = int(time.time())
        if data['f101'] != '' and data['f101'] not in dict_time:
            dict_time[data['f101']] = int(time.time())
    if data['mt'] == 1 and data['ec'] == 1402:
        if data['f64'] != '' and data['f64'] not in dict_time:
            dict_time[data['f64']] = int(time.time())
        if data['f82'] != '' and data['f82'] not in dict_time:
            dict_time[data['f82']] = int(time.time())
        if data['f102'] != '' and data['f102'] not in dict_time:
            dict_time[data['f102']] = int(time.time())
    if data['f63'] != '' and data['f63'] != 0:
        data['f63'] = dict_time[data['f63']]  # 整车当班上班时间
    if data['f64'] != '' and data['f64'] != 0:
        data['f64'] = dict_time[data['f64']]  # 整车当班下班时间
    if data['f81'] != '' and data['f81'] != 0:
        data['f81'] = dict_time[data['f81']]
    if data['f82'] != '' and data['f82'] != 0:
        data['f82'] = dict_time[data['f82']]
    if data['f101'] != '' and data['f101'] != 0:
        data['f101'] = dict_time[data['f101']]
    if data['f102'] != '' and data['f102'] != 0:
        data['f102'] = dict_time[data['f102']]
    # 删除多余数据
    for d in list(data.keys()):
        if data[d] == '':
            del data[d]
    return data


if __name__ == '__main__':

    equipmentNo = 1  # 设备数量
    # 通过多线程模拟设备数量，一个线程一个设备
    excel = ExcelUtils(DirPath.mqtt_data_xs_path, sheet_name="equipment")
    for i in range(equipmentNo):
        host = excel.readByCell(i + 1, 1)
        port = excel.readByCell(i + 1, 2)
        user = excel.readByCell(i + 1, 3)
        pwd = excel.readByCell(i + 1, 4)
        topic = excel.readByCell(i + 1, 5)
        sheet = excel.readByCell(i + 1, 6)
        print('*******************第' + str(i+1) + '个设备开始链接: ' + str(topic) + "*********************")
        thread = Thread(host, int(port), user, pwd, topic, sheet)
        thread.start()
        time.sleep(1)
