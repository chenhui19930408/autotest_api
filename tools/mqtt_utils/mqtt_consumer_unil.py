import paho.mqtt.client as mqtt

from config import setting


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(setting.mqtt_topic)


def on_message(client, userdata, msg):
    text = msg.payload.decode('utf-8')
    print(msg.topic + " " + ":" + str(text))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(setting.mqtt_host, setting.mqtt_port, 60)
client.username_pw_set(setting.mqtt_user, setting.mqtt_password)
client.loop_forever()
