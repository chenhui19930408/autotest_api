import random
import time
from datetime import datetime, timedelta
from time import sleep

import pymysql
import requests

from dir_path import DirPath

totle = 0
# log_file_name = './logs/' + time.strftime("%Y_%m_%d_%H_%M_%S") + '.txt'
log_file_name = DirPath.log_path + '/' + time.strftime("%Y_%m_%d_%H_%M_%S") + '.txt'
is_data_from_db = False
output = 50000
currentLength = 0


class DB:

    def __init__(self, host, port, username, password, basename):
        self.db = pymysql.connect(host=host, port=port, user=username, password=password,
                                  database=basename, charset='utf8')
        write_logs("链接数据库成功")

    def execute(self, sql):
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor

    def select(self, sql):
        cur = self.execute(sql)
        self.db.close()
        return cur.fetchall()


def write_logs(txt):
    with open(log_file_name, 'a') as file_object:
        file_object.write(txt + '\n')
        print(txt)


def get_db_data_by_time(time_from, time_to):
    # 测试环境
    db = DB(host='192.168.22.6', port=3306, username='spinningmes_qa',
            password='To2E8lXva5u1GrOGiyoky4xZhe1iv9Cw', basename='spinningmes_collect')
    write_logs("检索数据库-开始日期:  " + time_from + ", 结束日期:  " + time_to)
    db_data = db.select('SELECT * FROM xs_data WHERE create_time BETWEEN "'+time_from+'" AND "'+time_to+'" order by id')
    write_logs("检索成功，共 " + str(len(db_data)) + " 条数据")
    return db_data


def post_iot_data():
    start_time = datetime(2021, 7, 27, 21, 57, 46)
    end_time = (start_time + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')
    while True:
        data_list = get_db_data_by_time(str(start_time), str(end_time))
        if len(data_list) == 0:
            break
        else:
            for i in range(len(data_list)):
                sleep(2)
                global totle
                totle = totle + 1
                write_logs("共发送: " + str(totle) + "条，目前发送: "+str(i+1)+"/"+str(len(data_list)))
                post_data(db_to_json(data_list[i]))
        start_time = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S")
        end_time = (start_time + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M:%S')


def post_data(data):
    post_url = 'https://qa-i.szzhijing.com/spinning_zhongtai_back_collector/collector/xs'
    json = data
    header = {
        'corp-code': 'default',
        'orgcode': 'default'
    }
    r = requests.session().post(url=post_url, json=json, headers=header)
    write_logs('post: ' + post_url + '出参: ' + str(json) + ',返回: ' + r.text + '\n')
    if not r.json()['message'] == 'success':
        write_logs('发送失败')


def db_to_json(db_data):
    if is_data_from_db:
        json = {
            "body": [
                {
                    "workshopId": str(db_data[1]),
                    "machineCode": str(db_data[3]),
                    "gatherTime": str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
                    "spindleSpeed": int(db_data[7]) if db_data[7] else None,
                    "frontRollerSpeed": int(db_data[8]) if db_data[8] else None,
                    "draftMultiple": float(db_data[9]) if db_data[9] else None,
                    "fixedLength": int(db_data[10]) if db_data[10] else None,
                    "currentLength": int(db_data[11]) if db_data[11] else None,
                    "twist": int(db_data[12]) if db_data[12] else None,
                    "output": float(db_data[15]) if db_data[15] else None,
                    "tex": int(db_data[13]) if db_data[13] else None,
                    "frontRollerDiameter": int(db_data[27]) if db_data[27] else None,
                    "shutdown": int(db_data[44]) if db_data[44] else None,
                    "ia": float(db_data[16]) if db_data[16] else None,
                    "tpf": float(db_data[20]) if db_data[20] else None,
                    "uab": float(db_data[21]) if db_data[21] else None,
                    "tae": float(db_data[24]) if db_data[24] else None,
                    "temp": int(db_data[31]) if db_data[27] else None
                }
            ],
            "corpCode": "default"
        }
        return json
    else:
        global output, currentLength
        output = output + 200
        currentLength = currentLength + 10
        if currentLength > 3100:
            currentLength = 0
        json = {
            "body": [
                {
                    "workshopId": "1005",
                    "machineCode": "XS0001",
                    "gatherTime": str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
                    "spindleSpeed": random.randint(8000, 10000),
                    "frontRollerSpeed": random.randint(150, 200),
                    "draftMultiple": random.randint(50, 60),
                    "fixedLength": 3100,
                    "currentLength": currentLength,
                    "twist": random.randint(800, 900),
                    "output": output,
                    "tex": 8.05,
                    "frontRollerDiameter": None,
                    "shutdown": 1,
                    "ia": 8.05,
                    "tpf": random.randint(9, 10) * 0.1,
                    "uab": random.randint(420, 425),
                    "tae": random.randint(700, 740),
                    "temp": None
                }
            ],
            "corpCode": "default"
        }
        return json


if __name__ == "__main__":
    post_iot_data()